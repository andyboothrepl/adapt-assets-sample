# Icons #

| Name      | SVG                         |
|-----------|-----------------------------|
| Created   | ![Created](Created.svg)     |
| Reporting | ![Reporting](Reporting.svg) |
| Settings  | ![Settings](Settings.svg)   |